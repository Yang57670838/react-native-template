export enum TransactionCategories {
  "all" = "all",
  "workRelatedExpense" = "workRelatedExpense",
  "salary" = "salary",
  "earn" = "earn",
  "cashPool" = "cashPool",
  "insurance" = "insurance",
  "other" = "other",
}

export interface TransactionCategoryItem {
  label: string;
  key: TransactionCategories;
  color: string;
}
