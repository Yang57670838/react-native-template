import type { NativeStackNavigationProp } from "@react-navigation/native-stack";
import type { RouteProp } from "@react-navigation/native";

export type RootStackNavigatorList = {
  Login: undefined;
  TransactionCategoryMenu: undefined;
  TransactionHistory: {
    transactionCategory: string;
  };
};

export type LoginScreenNavigationProp = NativeStackNavigationProp<
  RootStackNavigatorList,
  "TransactionCategoryMenu"
>;

export type TransactionCategoryMenuScreenNavigationProp =
  NativeStackNavigationProp<RootStackNavigatorList, "TransactionHistory">;

export type TransactionHistoryScreenNavigationProp = NativeStackNavigationProp<
  RootStackNavigatorList,
  "TransactionCategoryMenu"
>;

export type TransactionHistoryScreenRouteProp = RouteProp<
  RootStackNavigatorList,
  "TransactionHistory"
>;
