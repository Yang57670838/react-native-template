import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { UserStore } from "../../types/user";

const initialState: UserStore = {
  token: undefined,
};

const userSlice = createSlice({
  name: "user",
  initialState: initialState,
  reducers: {
    login: (state, action: PayloadAction<string>) => {
      state.token = action.payload;
    },
  },
});

export const userActions = userSlice.actions;
export default userSlice.reducer;
