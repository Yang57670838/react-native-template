import { useEffect, useState } from "react";
import { StyleSheet, ImageBackground, SafeAreaView } from "react-native";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import { useFonts } from "expo-font";
import { NavigationContainer } from "@react-navigation/native";
import { Provider } from "react-redux";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { RootStackNavigatorList } from "./types/nav";
import { init } from "./db/db";
import { store } from "./store";
import AppLoading from "expo-app-loading";
import TransactionCategoryMenu from "./screens/TransactionCategoryMenu/TransactionCategoryMenu";
import TransactionHistory from "./screens/TransactionHistory/TransactionHistory";
import Login from "./screens/Login/Login";

const Stack = createNativeStackNavigator<RootStackNavigatorList>();

export default function App() {
  const [dbInitialized, setDbInitialized] = useState<boolean>(false);
  const [fontLoaded] = useFonts({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
  });

  useEffect(() => {
    init()
      .then(() => {
        setDbInitialized(true);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);

  if (!fontLoaded || !dbInitialized) {
    return <AppLoading />;
  }
  return (
    <LinearGradient colors={["#33B8F5", "#A638F9"]} style={styles.appScreen}>
      <ImageBackground
        source={require("./assets/background-image.png")}
        resizeMode="cover"
        style={styles.backgroundImageScreen}
        imageStyle={styles.backgroundImage}
      >
        <StatusBar style="auto" />
        <SafeAreaView style={styles.mainContent}>
          <Provider store={store}>
            <NavigationContainer>
              <Stack.Navigator
                initialRouteName="Login"
                screenOptions={{
                  headerStyle: { backgroundColor: "#351401" },
                  headerTintColor: "white",
                  contentStyle: { backgroundColor: "#3f2f25" },
                }}
              >
                <Stack.Screen
                  name="TransactionCategoryMenu"
                  component={TransactionCategoryMenu}
                  options={{
                    title: "All Categories",
                  }}
                />
                <Stack.Screen
                  name="TransactionHistory"
                  component={TransactionHistory}
                />
                <Stack.Screen
                  name="Login"
                  component={Login}
                  options={{
                    title: "Login",
                  }}
                />
              </Stack.Navigator>
            </NavigationContainer>
          </Provider>
        </SafeAreaView>
      </ImageBackground>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  appScreen: {
    flex: 1,
  },
  backgroundImageScreen: {
    flex: 1,
  },
  backgroundImage: {
    opacity: 0.15,
  },
  mainContent: {
    flex: 1,
    justifyContent: "center",
  },
});
