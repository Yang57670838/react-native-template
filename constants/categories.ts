import {
  TransactionCategories,
  TransactionCategoryItem,
} from "../types/transaction";

export const categories: Array<TransactionCategoryItem> = [
  {
    label: "All",
    key: TransactionCategories.all,
    color: "#2D6BA9",
  },
  {
    label: "Work Related Expense",
    key: TransactionCategories.workRelatedExpense,
    color: "#65338B",
  },
  {
    label: "Employee Salary",
    key: TransactionCategories.salary,
    color: "#1C865E",
  },
  {
    label: "Earn",
    key: TransactionCategories.earn,
    color: "#B62D8A",
  },
  {
    label: "Cash Pool",
    key: TransactionCategories.cashPool,
    color: "#DE3719",
  },
  {
    label: "Insurance",
    key: TransactionCategories.insurance,
    color: "#FFFFFF",
  },
  {
    label: "Other Expense",
    key: TransactionCategories.other,
    color: "#D1C8B7",
  },
];
