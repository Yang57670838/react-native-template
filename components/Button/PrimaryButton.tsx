import { StyleSheet, Text, View, Pressable } from "react-native";
import { FC, useState } from "react";

export type OwnProps = {
  children: React.ReactNode;
  pressHandler: () => void;
};

const PromaryButton: FC<OwnProps> = ({ children, pressHandler }) => {
  return (
    <>
      <View style={styles.container}>
        <Pressable
          onPress={pressHandler}
          android_ripple={{ color: "white" }}
          style={({ pressed }) => pressed && styles.pressed}
        >
          <View style={styles.buttonText}>{children}</View>
        </Pressable>
      </View>
    </>
  );
};

export default PromaryButton;

const styles = StyleSheet.create({
  container: {
    margin: 4,
    overflow: "hidden",
    borderRadius: 28,
    paddingVertical: 16,
    paddingHorizontal: 32,
    elevation: 2,
    backgroundColor: "#007DBA",
  },
  pressed: {
    opacity: 0.5,
  },
  buttonText: {
    textAlign: "center",
    borderRadius: 28,
  },
});
