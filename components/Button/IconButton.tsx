import { StyleSheet, Pressable } from "react-native";
import { FC } from "react";
import { Ionicons } from "@expo/vector-icons";

export type OwnProps = {
  pressHandler: () => void;
  ionIconName: string;
  iconColor: string;
};

const IconButton: FC<OwnProps> = ({ pressHandler, ionIconName, iconColor }) => {
  return (
    <>
      <Pressable
        onPress={pressHandler}
        android_ripple={{ color: "white" }}
        style={({ pressed }) => pressed && styles.pressed}
      >
        <Ionicons name={ionIconName as any} size={30} color={iconColor} />
      </Pressable>
    </>
  );
};

export default IconButton;

const styles = StyleSheet.create({
  pressed: {
    opacity: 0.5,
  },
  buttonText: {
    textAlign: "center",
    borderRadius: 28,
  },
});
