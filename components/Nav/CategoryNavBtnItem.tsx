import { StyleSheet, View, Pressable, Platform } from "react-native";
import { FC } from "react";
import AppText from "../Text/AppText";
import Color from "../../constants/color";

export type OwnProps = {
  navHandler: (key: string) => void;
  title: string;
  navKey: string;
  backgroundColor?: string;
};

// show in a list as one of the category, as a button style, to do navigation purpose when click..
const CategoryNavBtnItem: FC<OwnProps> = ({ navHandler, title, navKey, backgroundColor }) => {
  const pressHandler = () => {
    navHandler(navKey);
  };
  return (
    <>
      <View style={styles.container}>
        <Pressable
          onPress={pressHandler}
          style={({ pressed }) => [
            styles.button,
            pressed ? styles.pressedButton : null,
          ]}
          android_ripple={{ color: "#ccc" }}
        >
          <View style={[styles.innerContainer, { backgroundColor: backgroundColor || "white" }]}>
            <AppText style={styles.text} bold>
              {title}
            </AppText>
          </View>
        </Pressable>
      </View>
    </>
  );
};

export default CategoryNavBtnItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 16,
    height: 150,
    borderRadius: 8,
    elevation: 4,
    shadowColor: "black",
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    // overflow hidden is for andriod ripple effect to work inside container, but it will kill IOS shadow effect
    overflow: Platform.OS === "android" ? "hidden" : "visible",
  },
  button: {
    flex: 1,
  },
  pressedButton: {
    opacity: 0.5,
  },
  innerContainer: {
    flex: 1,
    padding: 16,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    backgroundColor: "#D1C8B7",
  },
  text: {
    fontSize: 18,
  },
});
