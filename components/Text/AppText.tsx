import { StyleSheet, Text } from "react-native";
import { FC } from "react";

export type OwnProps = {
  children: React.ReactNode;
  bold: boolean;
  style?: any;
};

const AppText: FC<OwnProps> = ({ children, bold, style }) => {
  return (
    <>
        <Text style={[styles.text, bold && styles.boldFont, style]}>{children}</Text>
    </>
  );
};

export default AppText;

const styles = StyleSheet.create({
  text: {
    fontFamily: "open-sans",
  },
  boldFont: {
    fontFamily: "open-sans-bold",
  }
});
