import { StyleSheet, View } from "react-native";
import { FC } from "react";
import Color from "../../constants/color";
import AppText from "../Text/AppText";

export type OwnProps = {
  text: string;
  containerStyle?: any;
  titleStyle?: any;
};

const PageTitle: FC<OwnProps> = ({ text, containerStyle, titleStyle }) => {
  return (
    <>
      <View style={[styles.container, containerStyle]}>
        <AppText style={[styles.title, titleStyle]} bold>{text}</AppText>
      </View>
    </>
  );
};

export default PageTitle;

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    borderColor: Color.primary,
    padding: 24,
    maxWidth: '80%',
    width: 300,
  },
  title: {
    color: Color.primary,
    textAlign: "center",
    fontSize: 24,
  },
});
