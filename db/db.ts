import * as SQLite from "expo-sqlite";

export const database = SQLite.openDatabase("local.db");

export function init() {
  return new Promise<void>((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS insights (month TEXT PRIMARY KEY, cost REAL, earn REAL)",
        [],
        () => resolve(),
        (_, err): boolean | any => reject(err)
      )
    });
  });
}
