import { database } from "../db";

export interface FetchInsightsFilterPayload {
  month: string; // 202305 for example
}

// TODO: add filter based on UI
export function fetchInsights(insertInsightsPayload: FetchInsightsFilterPayload) {
  const promise = new Promise((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        `SELECT * from insights`,
        [],
        (_, result) => {
          console.log(result);
          resolve(result.rows._array);
        },
        (_, err): boolean | any => reject(err)
      );
    });
  });
}
