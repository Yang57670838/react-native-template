import { database } from "../db";

export interface InsertInsightsPayload {
  month: string; // 202305 for example
  cost: number;
  earn: number;
}

// not used in this app
export function insertInsights(insertInsightsPayload: InsertInsightsPayload) {
  const promise = new Promise((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        `INSERT INTO insights (month, cost, earn) VALUES (?, ?, ?)`,
        [
          insertInsightsPayload.month,
          insertInsightsPayload.cost,
          insertInsightsPayload.earn,
        ],
        (_, result) => {
          console.log(result);
          resolve(result);
        },
        (_, err): boolean | any => reject(err)
      );
    });
  });
}
