const regexEmail = /^[\w.-]+@([\da-zA-Z.-]+)\.([a-zA-Z.]{2,6})$/;
const regexPhoneAU = /^(?:\+?61)?(?:\(0\)[23478]|\(?0?[23478]\)?)\d{8}$/;

export const isValidEmail = (value: string): boolean => {
    return regexEmail.test(value);
};

export const isValidAustralianPhone = (value: string): boolean => {
    return regexPhoneAU.test(value);
};
