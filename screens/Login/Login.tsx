import {
  StyleSheet,
  TextInput,
  View,
  Alert,
  useWindowDimensions,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { FC, useState } from "react";
import { AntDesign } from "@expo/vector-icons";
import { useDispatch } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import { isValidEmail } from "../../utils/validate";
import { LoginScreenNavigationProp } from "../../types/nav";
import { userActions } from "../../store/user";
import PrimaryButton from "../../components/Button/PrimaryButton";
import PageTitle from "../../components/Title/PageTitle";
import Color from "../../constants/color";
import AppText from "../../components/Text/AppText";

const Login = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation<LoginScreenNavigationProp>();
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  // dynamic style
  const { width } = useWindowDimensions();
  const loginBoxgMarginTop = width < 380 ? 25 : 50;
  const loginBoxPadding = width < 380 ? 12 : 24;
  const loginInputFontSize = width < 380 ? 16 : 22;

  const emailEnterChangeHandler = (text: string) => {
    setEmail(text);
  };

  const passwordEnterChangeHandler = (text: string) => {
    setPassword(text);
  };

  const resetInput = () => {
    setEmail("");
    setPassword("");
  };

  const signinHandler = () => {
    if (isValidEmail(email)) {
      // TODO: get token from server
      dispatch(userActions.login("mock-token"));
      navigation.navigate("TransactionCategoryMenu");
    } else {
      Alert.alert("Warning", "Please enter a valid email.", [
        { text: "Okay", style: "destructive", onPress: resetInput },
      ]);
    }
  };

  const forgetCredentialsHandler = () => {};

  return (
    <ScrollView style={styles.screen}>
      <KeyboardAvoidingView style={styles.screen} behavior="position">
        <View style={styles.content}>
          <PageTitle
            text="Accounting"
            containerStyle={styles.headingContainer}
          />
          <View
            style={[
              styles.loginBox,
              { marginTop: loginBoxgMarginTop, padding: loginBoxPadding },
            ]}
          >
            <TextInput
              style={[styles.loginInput, { fontSize: loginInputFontSize }]}
              maxLength={100}
              keyboardType="email-address"
              autoCapitalize="none"
              value={email}
              onChangeText={emailEnterChangeHandler}
            />
            <TextInput
              style={[styles.loginInput, { fontSize: loginInputFontSize }]}
              maxLength={100}
              keyboardType="visible-password"
              autoCapitalize="none"
              value={password}
              onChangeText={passwordEnterChangeHandler}
            />
            <PrimaryButton pressHandler={signinHandler}>
              <View style={styles.btnBox}>
                <AppText style={styles.btnText} bold={false}>
                  Signin
                </AppText>
                <AntDesign name="login" size={24} color="white" />
              </View>
            </PrimaryButton>
            <PrimaryButton pressHandler={forgetCredentialsHandler}>
              <View style={styles.btnBox}>
                <AppText
                  style={[styles.btnText, styles.forgetCredentialsText]}
                  bold={false}
                >
                  Forget Credential
                </AppText>
                <AntDesign name="questioncircleo" size={24} color="white" />
              </View>
            </PrimaryButton>
          </View>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default Login;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  content: {
    alignItems: "center",
  },
  loginBox: {
    marginHorizontal: 24,
    backgroundColor: "#004165",
    borderRadius: 8,
    elevation: 4,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.25,
  },
  loginInput: {
    height: 50,
    borderBottomColor: Color.primary,
    borderBottomWidth: 2,
    color: Color.primary,
    marginVertical: 8,
    textAlign: "center",
    fontFamily: "open-sans",
  },
  headingContainer: {
    marginHorizontal: 24,
  },
  btnBox: {
    flexDirection: "row",
  },
  btnText: {
    marginRight: 5,
    color: "white",
  },
  forgetCredentialsText: {
    textDecorationLine: "underline",
  },
});
