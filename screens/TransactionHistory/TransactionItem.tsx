import { StyleSheet, View, Pressable } from "react-native";
import { FC, useState } from "react";
import TransactionDetailsModal from "./TransactionDetailsModal";
import AppText from "../../components/Text/AppText";
import Color from "../../constants/color";

export type OwnProps = {
  item: any;
};

const TransactionItem: FC<OwnProps> = ({ item }) => {
  const [modalStatus, setModalStatus] = useState<boolean>(false);
  const openDetailsModal = () => {
    setModalStatus(true);
  };
  return (
    <>
      <View style={styles.transactionItemContainer}>
        <Pressable
          onPress={openDetailsModal}
          android_ripple={{ color: "#dddddd" }}
          style={({ pressed }) => [
            pressed && styles.pressedItem,
            styles.itemContents,
          ]}
        >
          <AppText style={styles.transactionItem} bold>
            {item.shortDescription}: {item.amount}
          </AppText>
          <AppText style={styles.transactionItem} bold={false}>
            {item.datatime}
          </AppText>
        </Pressable>
      </View>
      <TransactionDetailsModal
        item={item}
        isShow={modalStatus}
        setModalStatus={setModalStatus}
      />
    </>
  );
};

export default TransactionItem;

const styles = StyleSheet.create({
  transactionItemContainer: {
    marginVertical: 8,
    padding: 12,
    borderRadius: 6,
    backgroundColor: Color.listBoxBackground,
    elevation: 4,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3,
  },
  itemContents: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
  },
  transactionItem: {
    color: "white",
    padding: 8,
  },
  pressedItem: {
    opacity: 0.5,
  },
});
