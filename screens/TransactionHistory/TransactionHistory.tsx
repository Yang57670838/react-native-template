import { StyleSheet, View, Button, TextInput, FlatList } from "react-native";
import { useRoute, useNavigation } from "@react-navigation/native";
import { useState, useEffect } from "react";
import { categories } from "../../constants/categories";
import { TransactionCategories } from "../../types/transaction";
import {
  TransactionHistoryScreenRouteProp,
  TransactionHistoryScreenNavigationProp,
} from "../../types/nav";
import TransactionItem from "./TransactionItem";
import IconButton from "../../components/Button/IconButton";

// TODO
export const mockTransactionSearchResult: Array<any> = [
  {
    _id: "111",
    datatime: "2022-12-02",
    amount: "200",
    shortDescription: "coffee",
  },
  {
    _id: "45",
    datatime: "2024-2-02",
    amount: "200",
    shortDescription: "paper",
  },
  {
    _id: "333",
    datatime: "2021-12-02",
    amount: "10",
    shortDescription: "food",
  },
  {
    _id: "444",
    datatime: "2021-12-02",
    amount: "10",
    shortDescription: "food",
  },
  {
    _id: "555",
    datatime: "2021-12-02",
    amount: "10",
    shortDescription: "food",
  },
  {
    _id: "666",
    datatime: "2021-12-02",
    amount: "10",
    shortDescription: "food",
  },
  {
    _id: "777",
    datatime: "2021-12-03",
    amount: "10",
    shortDescription: "drink",
  },
  {
    _id: "888",
    datatime: "2021-12-04",
    amount: "10",
    shortDescription: "coffee",
  },
  {
    _id: "999",
    datatime: "2021-12-05",
    amount: "10",
    shortDescription: "kfc",
  },
];

const TransactionHistory = () => {
  const route = useRoute<TransactionHistoryScreenRouteProp>();
  const navigation = useNavigation<TransactionHistoryScreenNavigationProp>();
  const transactionCategoryKey = route.params?.transactionCategory;
  const [enteredSearhString, updateEnteredSearhString] = useState<string>("");

  const addNewTrans = () => {};

  useEffect(() => {
    navigation.setOptions({
      title: categories.find(
        (category) => category.key === transactionCategoryKey
      )?.label,
      headerRight: () => {
        return (
          <IconButton
            pressHandler={addNewTrans}
            ionIconName="add-circle"
            iconColor="white"
          />
        );
      },
    });
  }, [navigation, addNewTrans]);

  const changeSearchStringHandler = (text: string) => {
    updateEnteredSearhString(text);
  };

  const searchHandler = () => {
    console.log("updateEnteredSearhString", enteredSearhString);
  };
  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Transactions"
          style={styles.textInput}
          onChangeText={changeSearchStringHandler}
        />
        {/* // TODO: button font */}
        <Button title="Search" onPress={searchHandler} />
      </View>
      <View style={styles.transactionListContainer}>
        <FlatList
          keyExtractor={(item) => item._id}
          data={mockTransactionSearchResult}
          renderItem={(item) => {
            return <TransactionItem item={item.item} />;
          }}
          alwaysBounceVertical={false}
        />
      </View>
    </View>
  );
};

export default TransactionHistory;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 16,
  },
  inputContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    alignItems: "center",
    marginBottom: 24,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  textInput: {
    borderWidth: 1,
    borderColor: "#007dba",
    borderRadius: 6,
    backgroundColor: "#e4d0ff",
    color: "#120438",
    width: "70%",
    padding: 8,
    marginRight: 8,
    fontFamily: "open-sans",
  },
  transactionListContainer: {
    flex: 5,
  },
});
