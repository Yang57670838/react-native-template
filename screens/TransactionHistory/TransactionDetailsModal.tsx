import {
  StyleSheet,
  View,
  Modal,
  Button,
  Image,
  Pressable,
  useWindowDimensions,
} from "react-native";
import { FC, useState } from "react";
import AppText from "../../components/Text/AppText";

export type OwnProps = {
  item: any;
  isShow: boolean;
  setModalStatus: (status: boolean) => void;
};

const TransactionDetailsModal: FC<OwnProps> = ({
  item,
  isShow,
  setModalStatus,
}) => {
  const [textContentViewStatus, setTextContentViewStatus] =
    useState<boolean>(true);
  const [imageViewStatus, setImageViewStatus] = useState<boolean>(false);

  const { width, height } = useWindowDimensions();

  const imageWidth = width < 380 ? 200 : 300;
  const imageHeight = width < 380 ? 500 : 600;

  const closeModal = () => {
    setModalStatus(false);
  };

  const openImageView = () => {
    setImageViewStatus(true);
    setTextContentViewStatus(false);
  };

  const closeImageView = () => {
    setImageViewStatus(false);
    setTextContentViewStatus(true);
  };
  return (
    <>
      <Modal animationType="slide" visible={isShow}>
        <>
          {imageViewStatus && (
            <Pressable
              onPress={closeImageView}
              android_ripple={{ color: "#ccc" }}
              style={({ pressed }) => (pressed ? styles.imagePressed : null)}
            >
              <View style={styles.imageViewContainer}>
                <Image
                  style={[styles.image, {width: imageWidth, height: imageHeight}]}
                  source={require("../../assets/receipt_sample.png")}
                />
              </View>
            </Pressable>
          )}
          {textContentViewStatus && (
            <View style={styles.textViewContainer}>
              <View style={styles.details}>
                <AppText bold={false} style={styles.text}>
                  {item.shortDescription}
                </AppText>
                <AppText bold={false} style={styles.text}>
                  {item.amount}
                </AppText>
                <AppText bold={false} style={styles.text}>
                  {item.datatime}
                </AppText>
              </View>
              <View>
                <Button title="Open Document" />
                <Button title="Open Image" onPress={openImageView} />
              </View>
              <View>
                <Button title="Close" onPress={closeModal} />
              </View>
            </View>
          )}
        </>
      </Modal>
    </>
  );
};

export default TransactionDetailsModal;

const styles = StyleSheet.create({
  textViewContainer: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 16,
    justifyContent: "space-around",
  },
  imageViewContainer: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 16,
    margin: 16,
    backgroundColor: "white",
  },
  image: {
    margin: 20,
  },
  imagePressed: {
    opacity: 0.5,
  },
  details: {
    flexDirection: "row",
    justifyContent: "center",
    padding: 8,
  },
  text: {
    marginHorizontal: 4,
  },
});
