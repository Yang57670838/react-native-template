import { StyleSheet, View, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FC, useState } from "react";
import { categories } from "../../constants/categories";
import { TransactionCategoryMenuScreenNavigationProp } from "../../types/nav";
import TransactionHistory from "../TransactionHistory/TransactionHistory";
import CategoryNavBtnItem from "../../components/Nav/CategoryNavBtnItem";

const TransactionCategoryMenu = () => {
  const navigation = useNavigation<TransactionCategoryMenuScreenNavigationProp>();
  const navHandler = (key: string) => {
    navigation.navigate("TransactionHistory", {
      transactionCategory: key,
    });
  };
  return (
    <>
      <View>
        <FlatList
          keyExtractor={(item) => item.key}
          data={categories}
          numColumns={2}
          renderItem={(item) => {
            return (
              <CategoryNavBtnItem
                title={item.item.label}
                navKey={item.item.key}
                navHandler={navHandler}
                backgroundColor={item.item.color}
              />
            );
          }}
          alwaysBounceVertical={false}
        />
      </View>
    </>
  );
};

export default TransactionCategoryMenu;

const styles = StyleSheet.create({});
